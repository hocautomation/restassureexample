import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;


public class Sample {

    @Test
    public void TestDemo(){
        String endPoint = "http://demo.testfire.net/api/login";
        Response response = given().when().get(endPoint).then().extract().response();
        System.out.println(response);
        Assert.assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void TestDemo2(){
        String endPoint = "http://demo.testfire.net/api/login";
        Response response = given().when().get(endPoint).then().extract().response();
        System.out.println(response);
        Assert.assertEquals(response.getStatusCode(), 200);
    }
}
