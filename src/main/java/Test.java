import io.restassured.response.Response;

import static io.restassured.RestAssured.given;


public class Test {
    public static void main(String[] args) throws InterruptedException {
            String endPoint = "http://demo.testfire.net/api/login";
            Response response = given().when().get(endPoint).then().extract().response();
            System.out.println(response);
    }
}
